# Get absolute path to each module
cwd=$(shell pwd)
client=$(cwd)/modules/client
contracts=$(cwd)/modules/contracts
proxy=$(cwd)/modules/proxy
subgraph=$(cwd)/modules/subgraph

# Get src (ie prereqisites) for each module
find_options=-type f -not -path "*/node_modules/*" -not -name "*.swp"
client_src=$(shell find $(client)/src $(client)/ops $(find_options))
contract_src=$(shell find $(contracts)/contracts $(contracts)/migrations $(contracts)/ops $(find_options))
proxy_src=$(shell find $(proxy) $(find_options))
subgraph_src=$(shell find $(subgraph)/src $(subgraph)/ops $(find_options))

# Define docker image names for each module
app=alchemy
cacher_image=$(app)_cacher
client_image=$(app)_client
ethprovider_image=$(app)_ethprovider
graph_image=$(app)_graph
proxy_image=$(app)_proxy

# Define the docker runtime for each module
docker_run_in_client=docker run --name=buidler --tty --rm --volume=$(client):/app builder:dev
docker_run_in_contracts=docker run --name=buidler --tty --rm --volume=$(contracts):/app builder:dev
docker_run_in_subgraph=docker run --name=buidler --tty --rm --volume=$(subgraph):/app \
  --volume=$(contracts)/build:/contracts/build --network=ipfs builder:dev

# Use the version number from package.json to tag prod docker images
version=$(shell cat package.json | grep "\"version\":" | egrep -o "[.0-9]+")
net_version=4447

# Special variable: tells make here we should look for prerequisites
VPATH=ops:build:$(client)/build:$(client)/build/public:$(contracts)/build:$(subgraph)/build

# shortcut for various executable & helpful commands
webpack=./node_modules/.bin/webpack
graph_cli=./node_modules/.bin/graph
truffle=./node_modules/.bin/truffle
ipfs_start=docker run --name ipfs --network=ipfs --rm --detach --publish=5001:5001 ipfs/go-ipfs 2> /dev/null || true
ipfs_stop=docker container stop ipfs 2> /dev/null || true

# Env Setup
me=$(shell whoami)
$(shell mkdir -p build $(client)/build $(contracts)/build $(subgraph)/build)
$(shell docker network create ipfs 2> /dev/null || true)

####################
# Begin phony rules
.PHONY: default all dev prod clean stop purge deploy deploy-live

default: dev
all: dev prod
dev: proxy client cacher graph ethprovider
prod: proxy-prod cacher-prod graph-prod
clean:
	rm -rf build
	rm -rf $(client)/build
	rm -rf $(subgraph)/build

stop:
	docker container stop buidler 2> /dev/null || true
	docker container stop ipfs 2> /dev/null || true
	bash ops/stop.sh
	docker container prune --force

purge: clean stop
	rm -rf $(contracts)/build
	docker volume rm `docker volume ls -q | grep "[0-9a-f]\{64\}" | tr '\n' ' '` 2> /dev/null || true
	docker volume rm --force alchemy_cache_dev alchemy_chain_dev alchemy_graph_dev 2> /dev/null
	rm -rf $(client)/node_modules
	rm -rf $(contracts)/node_modules
	rm -rf $(subgraph)/node_modules

deploy: prod
	docker tag $(proxy_image):latest registry.gitlab.com/$(me)/alchemy/proxy:latest
	docker tag $(cacher_image):latest registry.gitlab.com/$(me)/alchemy/cacher:latest
	docker tag $(graph_image):latest registry.gitlab.com/$(me)/alchemy/graph:latest
	docker push registry.gitlab.com/$(me)/alchemy/proxy:latest
	docker push registry.gitlab.com/$(me)/alchemy/cacher:latest
	docker push registry.gitlab.com/$(me)/alchemy/graph:latest

deploy-live: prod
	docker tag $(proxy_image):latest registry.gitlab.com/$(me)/alchemy/proxy:$(version)
	docker tag $(cacher_image):latest registry.gitlab.com/$(me)/alchemy/cacher:$(version)
	docker tag $(graph_image):latest registry.gitlab.com/$(me)/alchemy/graph:$(version)
	docker push registry.gitlab.com/$(me)/alchemy/proxy:$(version)
	docker push registry.gitlab.com/$(me)/alchemy/cacher:$(version)
	docker push registry.gitlab.com/$(me)/alchemy/graph:$(version)

####################
# Begin Real Rules

# Proxy

proxy-prod: $(proxy_src) client.prod.js
	docker build --file $(proxy)/prod.dockerfile --tag $(proxy_image):latest .
	@touch build/proxy-prod

proxy: $(proxy_src)
	docker build --file $(proxy)/dev.dockerfile --tag $(proxy_image):dev .
	@touch build/proxy

# Alchemy client

client: client.dev.js
	docker build --file $(client)/ops/client/dev.dockerfile --tag $(client_image):dev $(client)
	@touch build/client

client.prod.js: client-node-modules $(client_src)
	$(docker_run_in_client) -c "webpack --config ops/client/webpack.prod.js"

client.dev.js: client-node-modules $(client_src)
	$(docker_run_in_client) -c "webpack --config ops/client/webpack.dev.js"

# Cacher server

cacher: cacher.dev.js $(client)/ops/cacher/dev.dockerfile $(client)/ops/cacher/entry.dev.sh
	docker build --file $(client)/ops/cacher/dev.dockerfile --tag $(cacher_image):dev $(client)
	@touch build/cacher

cacher-prod: cacher.prod.js $(client)/ops/cacher/prod.dockerfile $(client)/ops/cacher/entry.prod.sh
	docker build --file $(client)/ops/cacher/prod.dockerfile --tag $(cacher_image):latest $(client)
	@touch build/cacher-prod

cacher.dev.js: client-node-modules $(client_src) $(client)/ops/cacher/webpack.dev.js
	$(docker_run_in_client) -c "webpack --config ops/cacher/webpack.dev.js"

cacher.prod.js: client-node-modules $(client_src) $(client)/ops/cacher/webpack.prod.js
	$(docker_run_in_client) -c "webpack --config ops/cacher/webpack.prod.js"

client-node-modules: builder $(client)/package.json
	$(docker_run_in_client) /install.sh
	@touch build/client-node-modules

# Graph node & subgraph

graph: subgraph $(subgraph)/ops/entry.sh
	docker build --file $(subgraph)/ops/graph.dockerfile --tag $(graph_image):dev $(subgraph)
	@touch build/graph

graph-prod: subgraph-prod $(subgraph)/ops/entry.sh
	docker build --file $(subgraph)/ops/graph.dockerfile --tag $(graph_image):latest $(subgraph)
	@touch build/graph-prod

subgraph: sugraph-node-modules contract-artifacts $(subgraph_src) $(subgraph)/ops/build.sh
	$(ipfs_start)
	$(docker_run_in_subgraph) ops/build.sh 4447
	$(ipfs_stop)
	@touch build/subgraph

subgraph-prod: sugraph-node-modules contract-artifacts $(subgraph_src) $(subgraph)/ops/build.sh
	$(ipfs_start)
	$(docker_run_in_subgraph) ops/build.sh 1
	$(ipfs_stop)
	@touch build/subgraph-prod

sugraph-node-modules: builder $(subgraph)/package.json
	$(docker_run_in_subgraph) /install.sh
	@touch build/sugraph-node-modules

# Etherum provider: ganache + truffle 

ethprovider: contract-artifacts $(contract_ops)
	docker build --file $(contracts)/ops/dev.dockerfile --tag $(ethprovider_image):dev $(contracts)
	@touch build/ethprovider

contract-artifacts: contract-node-modules $(contract_src)
	$(docker_run_in_contracts) ops/build.sh
	@touch build/contract-artifacts

contract-node-modules: builder $(contract_src)
	$(docker_run_in_contracts) /install.sh
	@touch build/contract-node-modules

# builder

builder: ops/builder.dockerfile ops/install.sh
	docker build --file ops/builder.dockerfile --tag builder:dev .
	@touch build/builder
