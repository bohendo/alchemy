#!/bin/bash

set -e

# Not a js project? Don't do anything
if [[ ! -f package.json ]]
then echo "Couldn't find package.json, abort!" && exit 1
else echo "Install script activated!"
fi

# Have we ever installed these modules? If not, install them
if [[ ! -f yarn.lock ]]
then echo "No yarn.lock found, installing..." && yarn install
fi

# Setup cache folder & some helper vars/functions
mkdir -p .cache
hash="`cat package.json yarn.lock | sha256sum | tr -d ' -'`"
cache=".cache/$hash.tar.gz"
function create_cache { tar -cf - node_modules | pigz --fast > $cache; }

# The cache isn't up to date? We need to reinstall stuff
if [[ ! -f $cache ]]
then echo "No cache or node_modules found, installing..." && yarn install
     echo "Creating cache ${hash:0:8} ..." && create_cache

# No modules but the cache is up to date? Restore from cache
elif [[ -f $cache && ! -d node_modules ]]
then echo "Restoring from cache ${hash:0:8}..." && tar xzf $cache

# Both cache & node_modules are present? Nothing to do
else echo "Found cache ${hash:0:8} and node_modules, nothing to install..."
fi

echo "Done!"
