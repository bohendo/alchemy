FROM node:10.13-alpine

RUN mkdir -p /app
WORKDIR /app

# Install native build tools
RUN apk add --update --no-cache bash curl g++ gcc git jq libsecret-dev make pigz python
# Install JS build tools from
RUN yarn global add webpack webpack-cli webpack-dev-server nodemon

COPY ops/install.sh /install.sh

ENTRYPOINT ["bash"]
