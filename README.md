# Alchemy

## Overview

This is a monorepo that combines the power of:

 - [DAOstack's arc contracts](https://github.com/daostack/arc)
 - [An alchemy node server](https://github.com/daostack/daostack-node)
 - [The alchemy client](https://github.com/daostack/daostack)
 - A DAOstack subgraph

## How to get started developing

Prerequisites: docker, make, jq

The latter can be installed with `brew install make jq` or `sudo apt install make jq` if you don't have them already. Docker is a little tricker to install but instructions are available for [mac](https://docs.docker.com/docker-for-mac/install/) or [ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

With the prerequisites installed, all you have to do is run:

```
yarn start
```

**Warning: The very first time you run `yarn start` will take a very long time.** This is because it's going to download & build several docker images and install `node_modules` for several individual components. See performance considerations for further details.

Note that any `yarn` commands can also be run with `npm run`. Most of them can also be run by running the associated shell script eg `bash ops/logs graph`

I recommend you get your first `yarn start` started and then grab a coffee/beer and continue reading this readme while stuff installs & builds in the background.

## What happens when you run `yarn start`: a summary

`make dev` is run and then `ops/deploy.dev.sh` is executed.

**`make dev`**

 - If everything's up to date, then nothing happens and this step is skipped. If everything's up to date except one component, just that one component is built. Assuming nothing has been built yet...

 - Contract artifacts are extracted from those built-in to arc.js (`node_modules/@daostack/arc.js/migrated_contracts`). These are the source of truth regarding addresses of contracts deployed to mainnet. Then `yarn compile` updates the ABIs to fit the most up-to date source code for development.

 - The addresses from these contract artifacts are used to replace placeholders in the subgraph config (`modules/graph/src/subgraph.yaml`) so that these contract addresses can be updated dynamically during development. The `modules/graph/ops/build.sh` script is activated inside the `graph_builder` image to compile wasm binaries, upload files to ipfs, and then fetch the files from IPFS to be saved locally in `modules/graph/build/<network_id>`. A background ipfs process is automatically started via docker if not running already.

 - Several builder images are built from `ops/builder.dockerfile`. The `package.json` files from each module are copied into the image and `yarn install` is run to include `node_modules` as part of each component's builder image. This is done both for more powerful caching features and to prevent end users from needing to have various build tools installed.

 - Various JS bundles are built with webpack inside these builder images. For example, the client `src`, `ops`, and `build` directories are mounted into the `client_builder` docker image and webpack repackages the code in `src` into a bundle that will be available afterwards in `modules/client/build`.

 - Build artifacts (eg webpack bundles, graph config files from IPFS) are used to build docker images for each Alchemy component.

**`ops/deploy.dev.sh`**

 - Necessary images are pulled, secrets (eg for database password) are generated, and an embedded `docker-compose.yml` file is used to deploy a docker swarm.

 - The `ethprovider` executes `modules/contracts/ops/entry.sh` which will start ganache and then run any necessary migrations. A background process will watch both contract source code and migrations and, upon any changes, will recompile and remigrate as needed to keep Ganache up to date with the migrations & contract source code.

 - The `graph` service executes `modules/graph/ops/entry.sh` which will ensure that the necessary config files have been added to IPFS before starting the graph node

 - Client and cacher services are started. Both have `modules/contracts/build` bind-mounted into their copy of `arc.js` so that if the contracts are updated & the `ethprovider` service recompiles them, they will see the updates to the ABI in real-time. Webpack-dev-server and nodemon are used to make sure that either of these processes restart anytime their source changes.

 - The server will execute `modules/server/ops/entry.sh` which checks the database. If no tables are present, the db migrations will be executed to initialize the database and then the server is started.

## Tweak-Check Cycles

All tweak-check cycles start with `yarn start` which does an initial build and then starts the application in developer-mode.

#### Client code

Visit `localhost` and monitor the part of the UI that interests you. Make sure metamask is unlocked & listening to `localhost:8545`.

Edit a file in `modules/client/src` and save. The webpack-dev-server will recompile only what's needed to dynamically update the UI in your browser w/out needing a refresh.

Monitor the client logs with `yarn logs client` to ensure things get rebuilt as needed.

#### The Cacher

Edit a file in `modules/client/src` and save. A webpack process running in the background of the cacher service will automatically recompile the cacher's source and nodemon will restart the service.

Monitor the logs with `yarn logs cacher`.

#### Contracts & Migrations

Edit a file in `modules/contracts/contracts` or `modules/contracts/migrations`. A process running in the background of the ethprovider service will automatically recompile and remigrate.

Monitor the compilation/migration logs with `yarn logs ethprovider`.

View the ganache logs with `docker exec alchemy_ganache.1.<tab> cat ganache.log` where `<tab>` is used to trigger autocomplete for the container's id.

#### Explorer

Tweak a file in `modules/explorer/src`.

Run `yarn restart proxy` and, when this finishes, check the changes you made in the browser at `localhost/explorer`

#### The Graph

Tweak a file in `modules/graph/src`.

Run `yarn restart graph && yarn logs graph` to rebuild the graph node and then check the logs.

#### Server

Tweak a file in `modules/server/src`.

The server service will automatically detect this change and nodemon will restart to run the latest version of the code. Run `yarn logs server` to check the changes.

#### DevOps

This includes editing docker files, the makefile, entry scripts, and most other files that aren't in a path that looks like `modules/**/src`.

These changes will need a `yarn restart` to be reflected. Occasionally, if only a single process is affected by this change then `yarn restart <service_name>` will work.

In rare cases where the state of the app gets out of wack, you may need to run `make purge`. This will stop the app, destroy all dev-mode data and reset the app to a completely clean slate.

# Performance Considerations

This app is especially memory/cpu hungry if you're not running Linux since this whole application runs via Docker (which requires a linux kernel) so Mac machines have the additional overhead of running a linux VM.

But fear not, there **is** room for improvement. Upcoming performance upgrades are listed below:

 1. *rm Cacher*: A massive performance improvement will come when we can remove the `cacher` service. This is an extremely expensive process that's effectively doing the same thing as a graph node. Because we've already introduced the graph node, this app is temporarily much heavier than it needs to be due to redundant functionalities being present in both the cacher and the graph services. Once the client has been upgraded to pull it's blockchain cache from the graph node rather than the cacher, the cacher service can be removed bringing massive performance benefits.

 2. *rm Server*: The server is scheduled for removal and both the `server` and `server_db` services can be removed once we're able to store proposal data in IPFS. Again, we are temporarily in a position where the app is heavier than it needs to be because we've introduced redundant functionality with an IPFS node before removing the server & it's associated database. Once we've upgraded the client to use IPFS instead of this private data server, we can remove it. This server is a much more lightweight process than the cacher but, since it also requires a database, there should be noticable per

# Contract Cheatsheet

### [DAOToken](https://etherscan.io/address/0x543Ff227F64Aa17eA132Bf9886cAb5DB55DCAddf)
(Global)
GEN Token
Registered as the staking token in the GenesisProtocol contract.

### [UController](https://etherscan.io/address/0xa50c298118e89e0cc465f2a1705c7579f67b6f26)
(Global)
Owner of the Alchemy avatar and many others. The Avatar is registered here along with it's Reputation & Token addresses.
This contract manages which schemes & global constraints are registered with avatars.

### [GenesisProtocol](https://etherscan.io/address/0x8940442e7f54e875c8c1c80213a4aee7eee4781c)
(Global)
Voting machine that powers Alchemy.
Contains staking logic for boosting proposals and voting logic for passing proposals (binary yes/no outcomes).
When a proposal passes, the scheme that was being voted on gets executed. In Alchemy's case, this scheme is ContributionReward.
(part of infra, not arc)

### [ContributionReward](https://etherscan.io/address/0xc282f494a0619592a2410166dcc749155804f548)
(Global)
A scheme registered to Alchemy, it allows members to propose paying some amount of Eth or ERC20 to some beneficiary address

### [Redeemer](https://etherscan.io/address/0x67b13f159ca093325554aac6ee104fce36f3f9dd)
(Global)
Helper contract that lets you redeem your GEN from staking, Reputation for voting, and your contribution reward if any all in one clean transaction.
Linked to ContributionReward and GenesisProtocol.

### [Avatar](https://etherscan.io/address/0xa3f5411cfc9eee0dd108bf0d07433b6dd99037f1)
(Local)
The address of your DAO according to the rest of the ecosystem. The avatar's balance is your DAOs balance. It is controlled by the UController. It is tied to a native reputation and token but I don't think either of these are significant..

**Alchemy Schemes**:
Search [UController events](https://etherscan.io/address/0xa50c298118e89e0cc465f2a1705c7579f67b6f26#events) with this topic hash:
`keccak('RegisterScheme(address,address,address)') = 0x521230fe5fa463e77907aa7d9653b8cf93661e82561e966af8aa0a99910554c1`

Schemes registered to the Alchemy Avatar:
 - [Owner](https://etherscan.io/address/0x87ac6d6874c998f0f645f7db76b500d265c1856d#code)
 - [SchemeRegistrar](https://etherscan.io/address/0xf7122bb9f34c1ffbdc961940ed6aa6000fbf3ec7#code)
 - [GlobalConstraintRegistrar](https://etherscan.io/address/0xe000e03d1f4eaa3f7eb33d3473e720ca1b44f145#code)
 - [UpgradeScheme](https://etherscan.io/address/0xd96e541d8559a45bb226dc5488fd38bdc15e9c84#code)
 - [ContributionReward](https://etherscan.io/address/0xc282f494a0619592a2410166dcc749155804f548#code)
 - [GenesisProtocol](https://etherscan.io/address/0x8940442e7f54e875c8c1c80213a4aee7eee4781c#code)
 - [MultiSigWallet](https://etherscan.io/address/0x5e682cb9b6dc4b0fd299908672839556485e992b#code)
 - [GlobalConstraintRegistrar](https://etherscan.io/address/0xe000e03d1f4eaa3f7eb33d3473e720ca1b44f145#code)

### [Reputation](https://etherscan.io/address/0x13ad61aa8f695ce64711a51a6feccb48a275aaf8)
(Local)
Alchemy's native reputation contract. This contract is a bit like an ERC20 contract minus any transfer methods.
It holds reputation balances & this address registered in the UController's reputations mapping.

### [Token](https://etherscan.io/address/0x1adb85aa742114815928a813bde3968614ea58fe)
(Local)
Alchemy's native token. Doesn't seem to be used for much.
Only one [mysterious transaction](https://etherscan.io/tx/0xf432356429da3efbabd08ecff4a4a7ff5a2c1bad889d8743e5ffd90f71628aee) seems to have interacted with these tokens.

### [ContractCreator](https://etherscan.io/address/0x4b637cc57be0082fb19f9814ab628955ece36339)
(Global)
Helper contract for deploying new controllers

### [DaoCreator](https://etherscan.io/address/0x87ac6d6874c998f0f645f7db76b500d265c1856d)
(Global)
Helper contract for deploying new organizations.
It can
 - deploy a new Avatar
 - link it to a controller
 - register schemes
 - mint native tokens & reputation for founders
