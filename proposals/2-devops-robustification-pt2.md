# DevOps Robustification Project (part 2)

## Tokens Requested
 - 6 ETH - Food & shelter & student loan payments during an expected 30 hours of work
 - 0.5% Reputation - to keep up w reputation inflation

### About me

https://bohendo.com/hello-daostack/

## TL;DR

The outcome of this proposal will be:

 - Both Shivani's DAO explorer & Alchemy will become much easier to deploy to production meaning bug fixes & upgrades can be deployed in minutes instead of days.
 - I will deploy a more stable version of Shivani's DAO explorer alongside a back-up instance of Alchemy.
 - I will write a how-to guide for developing & deploying alchemy.

## Background

Part 1 of the DevOps Robustification project was officially completed on Oct 12th when I was able to build & deploy an independent Alchemy instance in developer-mode.

In the week since, I hacked together a quick deployment for Shivani's DAO explorer and worked to incorporate this explorer into alchemy's build/deploy scripts.

The first deployment of the DAO explorer was done in a rush before one of the pollinator weekly meetings. Many steps were done by hand & the whole process was disorganized & subject to human error. As a result, this version of the DAO explorer crashed several times and the process of deploying fixes was so difficult that it eventually died for good..

I think it's time to robustify the production environment for this DAO explorer & Alchemy.

## Proposal Overview

The goal of this proposal is to entirely automate the deployment of both Alchemy and the DAO explorer to production.

This will involve a single command that builds a production version of these projects and then uploads a copy to a globally-accessible registry. From this registry, it will be trivial to throw a script onto any random server that will download & deploy the latest version of Alchemy and/or the DAO explorer.

To make an easy deployment even more straightforward, I will write a short & simple [how-to guide](https://www.divio.com/blog/documentation/) outlining the steps needed for anyone else to deploy one for themself. The target audience will be open source devs who want to contribute to developing DAO user interfaces.

I will re-deploy the DAO explorer in a more stable form where deploying bug fixes or upgrades can happen much more quickly & easily. I will also deploy another instance of Alchemy that could serve as a back-up in case the main instance goes down or has caching problems.

## How does this proposal further Alchemy's Objectives

#### **First, it furthers decentralization and safety**

I envision a future where organization XYZ realizes that a DAO would greatly aid them in accomplishing some goal. They do some research & learn that DAOstack is the DAO platform that's most flexible & easiest to work with, so they deploy a new DAO and spin up a personalized alchemy-based UI for themselves.

Some organizations are trying to build a brand and want to host a UI for their DAO under their domain name with their own brand-specific style. I expect these orgs would appreciate being able to run their own UI under `dao.xyz.com` rather than relying on `alchemy.daostack.io/#/dao/0xuglyHash`.

Other organizations might not trust some random DAOstack sysadmin to keep the service up in a safe & reliable way. If & when DAOs start handling millions of dollars, the server behind `alchemy.daostack.io` becomes a single-point of failure. With the possibility of changing proposal data to mislead voters, it would also become a honey-pot drawing motivated attackers if it remains the only available UI (see next proposal for more details).

I expect the ecosystem will be able to grow much faster & will remain safer if a single technically savvy user can deploy a DAO **along with a UI** without needing to even look at any code. Easy DAO deployment is still on the todo list but this proposal will provide the tools & documentation needed to easily deploy new DAO user interfaces.

#### **Second, a reliable explorer will provides much-needed transparency.**

Hopefully, most of you were able to take advantage of the explorer sneak peak and enjoyed having access to info that's not provided by Alchemy yet.

The ATF (accountability task force) in particular, has concluded that a more information-rich explorer is essential for keeping track of the organization and it's members. 

Building this explorer was awesome (good job, Shivani) but it can't provide value to the community if it keeps crashing. Robustifying it's deployment in production is an important step to make sure the DAO explorer grows to provide as much value as possible.

#### **Third, It lays the groundwork for a mission-critical Alchemy upgrade**

It's no secret that Alchemy's handling of blockchain data leaves much to be desired. It relies on an extremely elaborate caching server that's error prone & slow to load. Alchemy also will not work without unlocking metamask, excluding any potential users who aren't already crypto-crazed tech savvy folks like us.

**Being able to browse proposals without unlocking metamask is mission critical.** Accomplishing this will come with the side benefit of slashing loading times to a fraction of what they currently are.

Because this proposal will deploy the DAO explorer alongside Alchemy, this version of alchemy automatically has access to all the data that the DAO explorer does. And this is data that's available without any connection to metamask required.

All we'd need after this proposal is finished is to overhaul the Alchemy client code to use the data from this explorer API rather than from a web3 connection. Big task, but this proposal is a critical step on the path to the holy grail of a metamask-less DAO browsing experience.

Then I'll finally be able to show my Mom what I've been up to :)

## Timeline

I've already spent a significant chunk of the last week laying the groundwork for this proposal.

 - Oct 23rd: Assuming this proposal is well on it's way to being passed, I'll start working early & may have something cool to show for it by the next Pollinator's meeting.

 - Oct 30th: DevCon4 begins. This is my internally-imposed deadline. I want to have published documentation showcasing how easy it is to develop with DAOstack & Alchemy that I can point to as I mingle with other developers at DevCon4.

## Future iterations

Unfortunately, deploying an independent version of Alchemy doesn't get us much because each instance stores proposal data privately. Once it's live, you'll notice that the back-up Alchemy instance is missing titles & links for most proposals. Or, if you submit a proposal on the back-up version, that the main instance is missing this data.

The journey towards a truely decentralized alchemy app is not over yet. See my next proposal regarding the decentralization of proposal data for the exciting conclusion.

