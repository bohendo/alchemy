# Decentralize Proposal Metadata

## Tokens Requested
 - 10 ETH - Pay for food & shelter during an expected 40-60 hours of work
 - 0.5% Reputation - to keep up w reputation inflation

### About me

See: bohendo.com/hello-daostack

## TL;DR

Move proposal data from a private server to IPFS.

## Summary

Proposal data (title, url) is currently being stored on a private server. If something goes wrong, this data could be lost forever. If a 3rd party app wants to build another DAO client, they are dependent on this private data repo to stay online & give them permission to access it. By storing this data on IPFS instead, data is far less likely to be lost forever and there is absolutely nothing stopping 3rd party developers from innovating on top of this platform & it's data.

## Background

"One simple litmus test for whether or not a blockchain project is truly decentralized: can a third party independently make a client for it, and do everything that the "official" client can?" - [Vitalik Non-giver of Ether](https://twitter.com/vitalikbuterin/status/1020661669079519233?lang=en)

Alchemy, the app built on top of DAOstack, is not decentralized according to Vitalik's litmus test yet. I have deployed another copy of Alchemy to alchemy.bohendo.com but it does not have access to proposal data (title & url) as this data is not stored in a decentralized way.

Currently, when you submit a proposal, the title + url is hashed & that fingerprint is submitted to the blockchain while the actual data is stored privately. The hash on the blockchain would help us identify that some title+url is correct but it can't help us get that info if we don't have it already. Data stored privately is therefore a dangerous single-point of failure. Even the professional sysadmins at Amazon have been known to [accidentally delete production data](https://aws.amazon.com/message/680587/) so this is a risk that we should take seriously even assuming that the sysadmins protecting this data are benevolent.

Putting proposal data on IPFS makes this dangerous single point of failure significantly less dangerous by allowing all DAOstack Apps to contain a complete copy of the proposal data & effortlessly share it with each other. If something terrible happens to one DAO's data, it can be recovered from the IPFS node of another DAO.

As many of you might know, DutchX is a decentralized exchange that's powered by DAOstack allowing members to vote on various parameters that control the exchange. If/when they release an Alchemy-style UI for their DAO, it and alchemy.daostack.io will be unable to share data with each other. When a proposal is submitted to the DutchX DAO app, the UI hosted by DAOstack will not have access to it. I'd love it if the UI provided by DAOstack could let me browse proposals from all DAOs in the ecosystem. Those who would like to get heavily involved in the proposal-prediction-market as stakers would probably appreciate this too.

Putting proposal data on IPFS allows multiple DAO apps to share data & effortlessly stay in sync.

## Proposal Overview

The DevOps Robustification Project part 2 will include deploying an IPFS node along side the old alchemy servers which is an important step. Now, all that's left to do is teach the client how to put it's data onto IPFS and how to retrieve it.

The biggest problem with the implementation of this solution is dealing with all the data that's not on IPFS yet. The easiest solution would be to abandon this data during the migration to IPFS but I don't like the idea of losing old proposal titles.. But I also don't like the idea of hosting the private data server forever just to make sure this data is available.

I think a happy medium would be to have a fallback IPFS document. Each proposal would save an IPFS has as it's description hash on-chain. The client, upon trying to load details for some proposal, will try to fetch this data from IPFS. If the client can't find this data in IPFS, it will use a hard-coded fallback that will contain data for all the legacy data that wasn't originally stored on IPFS.

## Implications

Other random clients knowing that the stored hash points to an ipfs object is powerful. Deciding on fields that they can expect to find for each proposal is even more powerful. If we start using IPFS then the contents of the proposal are publicly known, immutable, and secured by the blockchain.

Imagine a scenario: We want to declare the DAO's goals and decide to vote on a proposal that links to a google doc with the goal: "This DAO aims to promote diversity & build a strong community."

There is a subsequent falling-out or maybe the owner of this document shorts the DAO's native token. They have complete control over google doc declaring the DAO's mission statement and can change it to say "This DAO aims to end humanity".

Once not just the link associated with proposals, but the proposal **content** is rendered immutable by the blockchain, we can start attaching more important info to proposals and rest assured that it cannot be changed until the DAO members vote to change it.

## Future Directions

Once the proposal content is secured by IPFS, we can add additional fields with more interesting functionality **without changing the underlying contracts**. Some ideas:

 - **Arbitrary documents**: eg a mission statement, goals, a legal document. This document will be completely immutable so we can rely on it for trustless, high-stakes coordination

 - **Proposal description**: so that when the description link breaks, the description of the proposal will still live on. Valuable for very long-term preservation of proposal descriptions so that this data lives on even after Google goes out of business & shuts down google docs.

 - **A Peepeth-sized TL;DR**: It would be awesome if we could help newcomers get a feel for what kind of proposals are important to a DAO by including 140-char summaries as part of the top-level overview, available at a glance.

 - **Whisper/status chat-room ID**: Use this to for proposal-specific real-time communication? Alchemy could even display an interface to this chat room in a way that looks like comments on the desktop. Think: an uncensorable version of Diqus.

## Timeline

 - Nov 13th: If things go insanely well, this proposal might be nearing completion. More likely: the bulk of the work will be done and I'll be cleaning up some remaining bugs & preparing to submit a merge request to the alchemy repo so the UI at daostack.io can implement this upgrade too. Ether way, I'll present an update at the weekly pollinator's call.

 - Nov 20th: This proposal will be done unless things go very poorly. At this point, I expect to have deployed this upgrade to alchemy.bohendo.com and should have submitted the needed code upgrades & documentation to the DAOstack team so that they can implement this upgrade as well. I should have something pretty cool to show off at this week's pollinator's call.

